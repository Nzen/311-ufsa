
public interface ExtendedDelta
{
	// in case I had implemented a graph type instead
	int evaluate( String input );
	void addDelta( String transition );
}
