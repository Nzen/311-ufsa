
import string

def deltas( lines, alpha, last_state, abbr ) :
	for state in range( 0, last_state ) :
		state = str( state )
		for char in abbr[ alpha ] :
			lines.append( '(' + state + " " + char + " " + state + ")\n" )
	return lines

def pr_alpha( lines, key, abbr ) :
	bind_to_one = ''
	for any in abbr[ key ] :
		bind_to_one += any + ' '
	bind_to_one += '\n'
	lines.append( bind_to_one )
	return lines

def build( ) :
	abbreviations = {
		'a..z' : string.ascii_lowercase,
		'A..Z' : string.ascii_uppercase,
		'a..Z' : string.ascii_letters,
		'0..9' : string.digits }
	lines = []
	lines = pr_alpha( lines, 'a..Z', abbreviations )
	lines = pr_alpha( lines, '0..9', abbreviations )
	lines = deltas( lines, 'a..Z', 8, abbreviations )
	lines = deltas( lines, '0..9', 12, abbreviations )
	return lines

def save( lines_maybe ) :
	out = open( "stock.txt", 'w' )
	for one in lines_maybe :
		out.write( str( one ) )#+ '\n' )
	out.close( )

save( build( ) )
#for n in build() :
#	print n
	