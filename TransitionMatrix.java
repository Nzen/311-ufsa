
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.HashMap;
import java.lang.NullPointerException;

// Really wanted to make a graph instead. only allocated time for the standard approach.

public class TransitionMatrix implements ExtendedDelta
{
	private int[][] table;
	private HashMap<Character, Integer> alpha;

	public TransitionMatrix( int w_states, String alphabet )
	{
		table = new int[w_states][ alphabet.length() - 1 ];
		for ( int[] row : table )
		{
			Arrays.fill( row, -1 ); // yay
		}
		saveAlphabet( alphabet );
	}
	
	private void saveAlphabet( String alphabet )
	{
		StringTokenizer letters = new StringTokenizer( alphabet, " " );
		alpha = new HashMap<Character, Integer>( alphabet.length() );
		for ( int ind = letters.countTokens() - 1; letters.hasMoreTokens(); ind-- )
		{
			alpha.put( letters.nextToken().charAt( 0 ), ind );
		}
	}

	@Override
	public void addDelta( String transition )
	{
		StringTokenizer tweezer = new StringTokenizer( transition, " ()," );
		int now = Integer.parseInt( tweezer.nextToken() );
		char how = tweezer.nextToken().charAt( 0 );
		int go = Integer.parseInt( tweezer.nextToken() );
		table[ now ][ ind( how ) ] = go;
	}

	@Override
	public int evaluate( String input )
	{
		int currState = 0;
		if ( input == "" )
			return currState;
		char symb;
		int nn;
		int lim = input.length();
		for( int pointer = 0; pointer < lim; pointer++ )
		{
			symb = input.charAt( pointer );
			nn = ind( symb );
			if ( nn < 0 )
				return nn;
			currState = table[ currState ][ nn ];
			if ( currState < 0 ) // trap
				return currState;
		}
		return currState;
	}

	private int ind( char symbol )
	{
		try
		{
			Integer last = alpha.get( symbol );
			return last.intValue();
		}
		catch ( NullPointerException expectable )
			{ return -1; }
	}
}
