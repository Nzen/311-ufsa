
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.IllegalStateException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Loader
{
	private Scanner input;

	public void open( String fileName )
	{
		try
		{
			input = new Scanner( new File( fileName ) );
		}
		catch ( FileNotFoundException fileNotFoundException )
		{
			System.err.println( "Error opening file, not found." );
			System.exit( 1 ); // or try again
		}
	}

	public boolean definitionNext( )
	{
		return input.hasNext( "//--//" );
	}
	
	public boolean moreLines( )
	{
		return input.hasNextLine();
	}

	public String readLine( )
	{
		try
		{
			return input.nextLine( );
		}
		catch ( NoSuchElementException elementException )
		{
			System.err.println( "File data improperly formed." );
			input.close();
			System.exit( 1 );
		}
		catch ( IllegalStateException stateException )
		{
			System.err.println( "Error reading from file." );
			System.exit( 1 );
		}
		return ""; // assert unreachable
	}

	public void close( )
	{
	if ( input != null ) // ie, if it hasn't been closed
		input.close();
	}
}