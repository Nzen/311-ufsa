# A universal finite state automaton

Reconfigurable Fsa to evaluate strings.

# Running instructions

Made with Java 1.7, elements should be backwards compatible to 1.5
* Unzip files into an arbitrary folder.
* Have the Java JDK & JRE in your system path for console access.
* Open a console.
* Change the directory to the arbitrary folder.
* enter "javac TestsUfsm.java"

The program accepts either the default included file, in.txt, or one specified via the command line. A separate file will need to be in the same folder as the unpacked zip. These are, alternately:
* either enter "java TestsUfsm" (default)
* or enter "java TestsUfsm replace_this_string_with_file_name"

The program prints its output to the console. Should you desire a less ephemeral view, redirecting the output will work. 
* Instead of above, enter "java TestsUfsm > replace_with_output_name"

# Input file format
[number of states; first is initial]
[final states, space delimited]
[alphabet symbols]
{
	transition block
	(state symbol next_state)
}
//--// [definition delimiter]