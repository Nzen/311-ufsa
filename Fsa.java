
import java.util.StringTokenizer;

public class Fsa
{
	private int[] success;
	private TransitionMatrix deltaDB;
	
	public Fsa( int states, String finalStates, String alphabet )
	{
		deltaDB = new TransitionMatrix( states, alphabet );
		success = new int[ finalStates.length() ]; // wasted space, but I'm null terminating anyway
		defineSuccess( finalStates );
	}

	private void defineSuccess( String finals )
	{
		if ( success.length == 1 )
		{
			success[ 0 ] = Integer.parseInt( finals );
			return;
		}
		StringTokenizer letters = new StringTokenizer( finals, " " );
		int ind = 0;
		for ( ; letters.hasMoreTokens(); ind++ )
			success[ ind ] = Integer.parseInt( letters.nextToken() );
		success[ ind ] = -1;
	}

	public boolean evaluateString( String fromClient )
	{
		int ends = deltaDB.evaluate( fromClient );
		if ( ends < 0 )
			return false;
		else
		{
			return checkSuccess( ends );
		}
	}
	
	private boolean checkSuccess( int lastState )
	{
		if ( success.length == 1 )
			return lastState == success[ 0 ];
		for ( int nn = 0; success[ nn ] >= 0; nn++ )
		{
			if ( success[ nn ] == lastState )
				return true;
		}
		return false;
	}
	
	public void saveTransition( String def )
	{
		deltaDB.addDelta( def );
	}
}
