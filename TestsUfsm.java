
import java.util.StringTokenizer;

public class TestsUfsm
{
	public static void main(String[] args)
	{
		String fileN = ( args.length > 0 ) ? args[0] : "in.txt";
		Ufsm project1 = new Ufsm( fileN );
		//test_transaction();
		//test_fsa();
	}
	
	public static void test_transaction()
	{
		TransitionMatrix banana = new TransitionMatrix( 2, "a b" );
		banana.addDelta( "(0 a 1)" );
		banana.addDelta( "(0 b 0)" );
		banana.addDelta( "(1 a 0)" );
		banana.addDelta( "(1 b 1)" );
		System.out.println( banana.evaluate( "aabbaabbaa" ) == 0 );
		System.out.println( banana.evaluate( "aaa" ) == 1 );
	}

	public static void test_fsa()
	{
		Fsa regex = new Fsa( 2, "0", "a b" );
		regex.saveTransition( "(0 a 1)" );
		regex.saveTransition( "(0 b 0)" );
		regex.saveTransition( "(1 a 0)" );
		regex.saveTransition( "(1 b 1)" );
		System.out.println( regex.evaluateString( "aaabbbaabbaa" ) );
		System.out.println( regex.evaluateString( "b" ) );
		System.out.println( regex.evaluateString( "aaa" ) );
	}
}
