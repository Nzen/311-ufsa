
import java.lang.StringBuilder;
import java.util.StringTokenizer;

public class Ufsm
{
	private Loader in;
	private Fsa machine;
	public Ufsm( String fileN )
	{
		in = new Loader();
		in.open( fileN );
		run();
		in.close();
	}
	
	// states, finals, sigma, transition block, evaluation block (//--// if more)
	private void run()
	{
		int states;
		String finalStates;
		String alphabet;
		String aTransition;
		String forEvaluation;
		int numDone = 0;
		do
		{
			numDone += 1;
			states = Integer.parseInt( in.readLine() );
			finalStates = in.readLine();
			alphabet = in.readLine();
			machine = new Fsa( states, finalStates, alphabet );
			
			emitHeader( numDone, states, finalStates, alphabet );
			
			aTransition = in.readLine();
			while( !aTransition.isEmpty() && aTransition.charAt( 0 ) == '(' )
			{
				machine.saveTransition( aTransition );
				System.out.println( '\t' + aTransition.substring( 1, aTransition.length() - 1 ) );
				aTransition = in.readLine();
			}
			System.out.println( "(5) strings:" );
			evalReport( aTransition );
			while ( in.moreLines() && !(in.definitionNext()) )
			{
				forEvaluation = in.readLine();
				evalReport( forEvaluation );
			}
			if ( in.definitionNext() )
			{
				in.readLine();
				System.out.println( );
			}
			else
				break;
		} while ( true );
	}
	
	private void emitHeader( int numDone, int states, String finalStates, String alphabet )
	{
		System.out.printf( "Final State Automaton #%d.\n", numDone );System.out.println();
		System.out.printf( "(1) number of states: %d\n", states );
		System.out.printf( "(2) final states: %s\n", withCommas( finalStates ) ); // COMMAs separating
		System.out.printf( "(3) alphabet: %s\n", withCommas( alphabet ) ); // COMMAs separating
		System.out.printf( "(4) transitions:\n" );
	}
	
	private String withCommas( String list )
	{
		StringBuilder knitter = new StringBuilder();
		StringTokenizer needles = new StringTokenizer( list, " ()" );
		while ( needles.hasMoreTokens() )
		{
			knitter.append( needles.nextToken() );
			knitter.append( ", " );
		}
		return knitter.substring( 0, knitter.length() - 2 );
	}
	
	private void evalReport( String line )
	{
		System.out.print( '\t' + line + "\t\t" ); 
		System.out.println( machine.evaluateString( line ) ? "Accept" : "Reject");
	}
}
